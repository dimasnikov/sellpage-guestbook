## Требования

1. Composer
2. PHP7.1
3. MySQL
4. NodeJS
5. WebServer

## Установка/Запуск

1. Клон репозитория
2. Настройка доступов в бд. Для этого переименовываем файл .env.example в .env и заменяем эти строчки на свои:
    > DB_CONNECTION=mysql
    > DB_HOST=127.0.0.1
    > DB_PORT=3306
    > DB_DATABASE=sellpage_guest_book
    > DB_USERNAME=test
    > DB_PASSWORD=test
3. Из корня проекта выполняем `php artisan key:generate`
4. Устанавливаем структуру таблиц `php artisan migrate`
5. Настраиваем видимость сервера в папку public/

## Рабочие файлы

1. app/Http/Controllers/`CommentController.php` - основная логика пхп
2. app/`Comment.php` - модель для удобства работы
3. database/migrations/`2020_01_29_014356_create_comments_table.php` - логика создания таблицы
4. routes/`api.php` - роутинг для апи
5. resources/js/components/`AddFormComponent.vue` - логика запроса на создание записи
6. resources/js/components/`CommentsComponent.vue` - логика вывода всех записей
7. resources/views/`welcome.blade.php` - основной рендеринг страницы
