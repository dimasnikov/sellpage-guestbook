<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Guest book</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

        <script src="{{ asset('/js/app.js') }}" defer></script>
    </head>
    <body style="background-color:#f8f9fa;">
        <div class="wrapper" id="app">
            <div class="container mt-4" >
                <div class="row mb-4"><h1 class="w-100 h1 text-center">Guest Book</h1></div>
                <div class="row">
                    <add-form-component></add-form-component>
                </div>
                <div class="row">
                <hr class="w-100 p-1">
                </div>
                <div class="row">
                    <comments-component></comments-component>
                </div>
            </div>
        </div>
    </body>
</html>
