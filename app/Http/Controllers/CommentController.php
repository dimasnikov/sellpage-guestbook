<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \App\Comment;

class CommentController extends Controller
{
    public function index()
    {
        return Comment::orderBy('id', 'desc')->get();
    }

    public function store(Request $request)
    {
        // используем валидатор вместо if блоков
        $validator = Validator::make($request->toArray(), [
            'username' => 'required',
            'description' => 'required|max:500',
        ]);

        if (!$validator->failed()) {
            $comment = new Comment;
            $comment->username = $request->get('username');
            $comment->comment = $request->get('description');
            $comment->saveOrFail();

            return response($comment->toJson());
        }

        return response($validator->errors, 403);
    }

    public function delete($id)
    {
        Comment::find($id)->delete();
        return response(['id' => $id]);
    }
}
