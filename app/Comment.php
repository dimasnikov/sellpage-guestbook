<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    protected $hidden = ['updated_at'];

    protected $casts = [
        'created_at' => 'datetime:H:i d.m.Y',
    ];
}
